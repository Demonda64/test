package exemple;

public class Voiture extends Vehicule {

	private String conduite;
	
	public Voiture() {
		
	}
	public Voiture(String c) {
		this.conduite = c;
	}
	
	public Voiture(int v, int r, int p, String e, String c) {
		super(v,r,p,e);
		this.conduite = c;
	}
	

	public String getConduite() {
		return conduite;
	}

	public void setConduite(String conduite) {
		this.conduite = conduite;
	}
	
	public void message() {
		super.message();
		String msg = "";
		msg += "Conduite : " + this.conduite +"\n";
		System.out.println(msg);
	}
}
