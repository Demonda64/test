package exemple;

public class Vehicule {

	// Propri�t�s
	private int vitesse;
	private int nbRoues;
	private int nbPlaces;
	private String energie;
	
	// Constructeurs
	public Vehicule() {
		
	}
	
	public Vehicule(int v, int roues, int places) {
		this.vitesse = v;
		this.nbRoues = roues;
		this.nbPlaces = places;
	}
	
	public Vehicule(int v, int r, int p, String e) {
		this.setVitesse(v);
		this.nbRoues = r;
		this.nbPlaces = p;
		this.energie = e;
	}
	
	// Setters
	public void setVitesse(int v) {
		if(v > 0) this.vitesse = v;
	}
	
	public void setNbRoues(int r) {
		this.nbRoues = r;
	}
	
	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}
	
	public void setEnergie(String energie) {
		this.energie = energie;
	}
	
	// Getters
	public int getVitesse() {
		return this.vitesse;
	}
	
	public int getNbRoues() {
		return this.nbRoues;
	}
	
	public int getNbPlaces() {
		return nbPlaces;
	}
	
	public String getEnergie() {
		return energie;
	}
	
	
	// Methodes
	
	public String seDeplacer() {
		return "je me d�place";
	}
	
	public void message() {
		String msg = "";
		msg += "Vitesse : " + this.vitesse +"\n";
		msg += "Nombre de roues : " + this.nbRoues +"\n";
		msg += "Nombre de places : " + this.nbPlaces +"\n";
		msg += "Energie : " + this.energie +"\n";
				
		System.out.println(msg);
	}




	
}
